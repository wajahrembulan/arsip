<?php
session_start();
if(isset($_SESSION["username"])=="admin");
	else
header('location:login.php');
$conn = new mysqli("localhost", "root", "", "arsip");
$sql = "select * from admin;";
$result = $conn->query($sql);
?>

<!DOCTYPE html>
<html>
<title>Ex: Aplikasi Arsip</title>
<body>


<style>
ul{
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #46add9; /* light blue */
}

li{
	display: inline;
	float: left;
}

li a{
	display: block;
	color: white;
	text-align: center;
    padding: 14px 16px;
    background-color: #46add9; /* light blue */
    text-decoration: none;
}

li a:hover{
	background-color: #317997; /* dark blue */
}

table {
	border-collapse: collapse;
	border: 1px solid gray;
}

td {
	padding: 6px;
}

th {
	padding: 6px;
	background-color: #008ab8; /* blue */
	color: white;
}

tr:nth-child(even) {
	background-color: #f2f2f2
}

.box {
	border-radius: 4px;
	width: 70%;
}   

.button {
	background-color: #008CBA; /* blue */	
	border: none;
	border-radius: 5px;
	color: white;
	padding: 5px 23px;
	font-size: 16px;
}	

.button-table-edit {
	background-color: #1ab2ff; /* blue */	
	border: none;
	border-radius: 4px;
	color: white;
	padding: 5px 23px;
	font-size: 15px;	
	text-decoration: none;
}

.button-table-hapus {
	background-color: #ff3333; /* red */	
	border: none;
	border-radius: 4px;
	color: white;
	padding: 5px 23px;
	font-size: 15px;	
	text-decoration: none;
}
}	
</style>

<ul>
<li><a href="index.php">HOME</a></li>
<li><a href="tambahadmin.php">TAMBAH ADMIN</a></li>
<li style="float:right"><a href="logout.php">LOGOUT</a></li>
</ul>

<br>
<br>

<center>
<table>
	<tr align="center">
		<th>USERNAME</th>
		<th>PASSWORD</th>
		<th>NAMA</th>
		<th>NIK</th>
		<th>BAGIAN</th>
		<th>AKSI</th>
	</tr>
	<?php
	while($row = $result->fetch_assoc()){
	?>
	<tr align="center">
		<td><?php echo $row["USERNAME"]; ?></td>
		<td><?php echo $row["PASSWORD"] ?></td>
		<td><?php echo $row["NAMA"] ?></td>
		<td><?php echo $row["NIK"] ?></td>
		<td><?php echo $row["BAGIAN"] ?></td>
		<td><a class="button-table-edit" href="editadmin.php?username=<?php echo $row["USERNAME"]; ?>">EDIT</a>
		<a class="button-table-hapus" href="hapusadmin.php?username=<?php echo $row["USERNAME"]; ?>">HAPUS</a></td>
		
	</tr>
	<?php
	}
	?>
</table>
</center>