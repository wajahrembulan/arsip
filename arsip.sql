/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     16/06/2017 13:10:01                          */
/*==============================================================*/


drop table if exists ADMIN;

drop table if exists ARSIP_TAHUNAN;

drop table if exists FILE_SURAT;

drop table if exists SURAT;

/*==============================================================*/
/* Table: ADMIN                                                 */
/*==============================================================*/
create table ADMIN
(
   USERNAME             char(20) not null,
   PASSWORD             char(20) not null,
   NAMA                 char(30),
   NIK                  char(30),
   BAGIAN               char(30),
   primary key (USERNAME)
);

/*==============================================================*/
/* Table: ARSIP_TAHUNAN                                         */
/*==============================================================*/
create table ARSIP_TAHUNAN
(
   USERNAME             char(20) not null,
   TAHUNSURAT           int not null,
   primary key (USERNAME, TAHUNSURAT)
);

/*==============================================================*/
/* Table: FILE_SURAT                                            */
/*==============================================================*/
create table FILE_SURAT
(
   USERNAME             char(20) not null,
   TAHUNSURAT           int not null,
   IDSURAT              int not null,
   NAMAFILE             char(255) not null,
   primary key (USERNAME, TAHUNSURAT, IDSURAT, NAMAFILE)
);

/*==============================================================*/
/* Table: SURAT                                                 */
/*==============================================================*/
create table SURAT
(
   USERNAME             char(20) not null,
   TAHUNSURAT           int not null,
   IDSURAT              int not null,
   NOAGENDA             char(20),
   NOSURAT              char(20),
   ASALTUJUAN           char(100),
   ISIRINGKAS           text,
   KODEKLARIFIKASI      char(20),
   TANGGALSURAT         date,
   TANGGAL              date,
   KETERANGAN           text,
   SURATMASUK           bool,
   DIPINJAM             bool,
   NAMAPEMINJAM         char(30),
   DIVISIPEMINJAM       char(30),
   TANGGALPEMINJAMAN    date,
   TANGGALPENGEMBALIAN  date,
   primary key (USERNAME, TAHUNSURAT, IDSURAT)
);

alter table ARSIP_TAHUNAN add constraint FK_RELATIONSHIP_2 foreign key (USERNAME)
      references ADMIN (USERNAME) on delete restrict on update restrict;

alter table FILE_SURAT add constraint FK_RELATIONSHIP_1 foreign key (USERNAME, TAHUNSURAT, IDSURAT)
      references SURAT (USERNAME, TAHUNSURAT, IDSURAT) on delete restrict on update restrict;

alter table SURAT add constraint FK_RELATIONSHIP_3 foreign key (USERNAME, TAHUNSURAT)
      references ARSIP_TAHUNAN (USERNAME, TAHUNSURAT) on delete restrict on update restrict;

