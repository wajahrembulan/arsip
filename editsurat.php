<?php
session_start();
if(isset($_SESSION["username"]));
	else
	header('location:login.php');
if(isset($_GET['idsurat'])){
	$conn = new mysqli("localhost", "root", "", "arsip");
	$sql = "select * from surat where idsurat='".$_GET['idsurat']."';";
	$result = $conn->query($sql);
	$row = $result->fetch_assoc();
}
if(isset($_POST['idsurat'])){
	$conn = new mysqli("localhost", "root", "", "arsip");
	$sql="update surat set noagenda='".$_POST['noagenda']."', nosurat='".$_POST['nosurat']."',asaltujuan='".$_POST['asaltujuan']."',isiringkas='".$_POST['isiringkas']."',kodeklarifikasi='".$_POST['kodeklarifikasi']."',tanggal='".$_POST['tanggal']."',keterangan='".$_POST['keterangan']."',dipinjam='".$_POST['dipinjam']."' where username='".$_SESSION['username']."' and tahunsurat='".$_SESSION['tahun']."' and idsurat='".$_POST['idsurat']."';";
	$conn->query($sql);
	if($_POST['dipinjam'])
		header('location:tambahpeminjam.php?idsurat='.$_POST['idsurat']);
	else
		header('location:surat.php?q='.$_POST['suratmasuk']);
}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Edit Surat</title>
</head>
<body>

<style>
ul{
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #46add9; /* light blue */
}

li{
	display: inline;
	float: left;
}

li a{
	display: block;
	color: white;
	text-align: center;
    padding: 14px 16px;
    background-color: #46add9; /* light blue */
    text-decoration: none;
}

li a:hover{
	background-color: #317997; /* dark blue */
}

table {
	border-collapse: collapse;
	border: 1px solid gray;
}

td {
	padding: 6px;
}

th {
	padding: 6px;
	background-color: #008ab8; /* blue */
	color: white;
}

tr:nth-child(even) {
	background-color: #f2f2f2
}

.box {
	border-radius: 4px;
	width: 70%;
}   

.button {
	background-color: green; /* green */	
	border: none;
	border-radius: 5px;
	color: white;
	padding: 5px 5px;
	font-size: 14px;
	text-decoration: none;
}	

.button-table-edit {
	background-color: #1ab2ff; /* blue */	
	border: none;
	border-radius: 4px;
	color: white;
	padding: 5px 23px;
	font-size: 15px;	
	text-decoration: none;
}

.button-table-hapus {
	background-color: #ff3333; /* red */	
	border: none;
	border-radius: 4px;
	color: white;
	padding: 5px 23px;
	font-size: 15px;	
	text-decoration: none;
}

.button-table-print {
	background-color: #33cc33; /* green */	
	border: none;
	border-radius: 4px;
	color: white;
	padding: 5px 23px;
	font-size: 15px;	
	text-decoration: none;
}

}
</style>


<ul>
	<li> <a href="surat.php?q=1">SURAT MASUK</a> </li>
	<li> <a href="surat.php?q=0">SURAT KELUAR</a> </li>
	<li > <a style="display:<?php if($_SESSION['username']!='admin')echo "none"; ?>" href="admin.php">KELOLA ADMIN</a> </li>
	<li style="float:right"> <a href="logout.php">LOGOUT</a> </li>
</ul>


<br>

<center>

<table border="1px">
<form method="POST" action="editsurat.php">
    <input name="idsurat" type="hidden" value="<?php if(isset($_POST["idsurat"])) echo $_POST["idsurat"];
    if(isset($_GET["idsurat"]) ) echo $row["IDSURAT"];?>"/>
	<tr>
		<td>NO AGENDA</td>
		<td> <input name="noagenda" type="text" class="box" value="<?php if(isset($_POST["noagenda"])) echo $_POST["noagenda"];if(isset($_GET["idsurat"]) ) echo $row["NOAGENDA"];  ?>"/></td>
	</tr>	
	<tr>
		<td>NO SURAT</td>
		<td> <input name="nosurat" type="text" class="box" value="<?php if(isset($_POST["nosurat"])) echo $_POST["nosurat"];if(isset($_GET["idsurat"]) ) echo $row["NOSURAT"];  ?>" /></td>
	</tr>
	<tr>
		<td><?php if($row['SURATMASUK'])echo "ASAL";else echo "TUJUAN"; ?></td>
		<td> <input name="asaltujuan" type="text" class="box" value="<?php if(isset($_POST["asaltujuan"])) echo $_POST["asaltujuan"];if(isset($_GET["idsurat"]) ) echo $row["ASALTUJUAN"];  ?>" /></td>
	</tr>
	<tr>
		<td>ISI RINGKAS</td>
		<td> <textarea rows="2" cols="20" name="isiringkas" class="box" type="text"><?php if(isset($_POST["isiringkas"])) echo $_POST["isiringkas"];if(isset($_GET["idsurat"])) echo $row["ISIRINGKAS"];?></textarea></td>
	</tr>
	<tr>
		<td>KODE KLARIFIKASI</td>
		<td><input name="kodeklarifikasi" type="text" class="box" value="<?php if(isset($_POST["kodeklarifikasi"])) echo $_POST["kodeklarifikasi"];if(isset($_GET["idsurat"]) ) echo $row["KODEKLARIFIKASI"];  ?>" /></td>
	</tr>
	<tr>
		<td>TANGGAL ARSIP</td>
		<td><input name="tanggal" type="date" class="box" value="<?php if(isset($_POST["tanggal"])) echo $_POST["tanggal"];if(isset($_GET["idsurat"]) ) echo $row["TANGGAL"];  ?>" /></td>
	</tr>
	<tr>
		<td>TANGGAL SURAT</td>
		<td><input name="tanggalsurat" type="date" class="box" value="<?php if(isset($_POST["tanggalsurat"])) echo $_POST["tanggalsurat"];if(isset($_GET["idsurat"]) ) echo $row["TANGGALSURAT"];  ?>" /></td>
	</tr>
	<tr>
		<td>KETERANGAN</td>
		<td><textarea rows="2" cols="20" name="keterangan" type="text" class="box"><?php if(isset($_POST["keterangan"])) echo $_POST["keterangan"];if(isset($_GET["idsurat"]) ) echo $row["KETERANGAN"];?></textarea> </td>
	</tr>
	<tr>
	    <td>STATUS</td>
	    <td>
	        <?php 	if(isset($_POST["dipinjam"])){
						?>
						<input type="radio" name="dipinjam" value="1" <?php if($_POST['dipinjam'])echo "checked"; ?>>Dipinjam</input>
						<input type="radio" name="dipinjam" value="0" <?php if(!$_POST['dipinjam'])echo "checked"; ?>>Tersedia</input>
						<?php
				}
				if(isset($_GET["idsurat"])){
						?>
						<input type="radio" name="dipinjam" value="1" <?php if($row['DIPINJAM'])echo "checked"; ?>>Dipinjam</input>
						<input type="radio" name="dipinjam" value="0" <?php if(!$row['DIPINJAM'])echo "checked"; ?>>Tersedia</input>
						<?php
				}
		    ?>
	    </td>
	</tr>
	<input type="hidden" name="suratmasuk" value="<?php if(isset($_GET["idsurat"])) echo $row["SURATMASUK"];
													if(isset($_POST["suratmasuk"])) echo $_POST["suratmasuk"]; ?>">
	<tr>
		<td colspan="2" align="center"> <input type="submit" value="Submit" class="button" /> </td>
		</form>
</table>
</center>

<br>
<br>

<center>
<table border="1px">

	<tr>
		<form method="POST" action="upload.php" enctype="multipart/form-data">
			<td>TAMBAH FILE</td>
			<td> <input type="file" name="fileToUpload[]" multiple /> </td>
			<input type="hidden" name="idsurat" value="<?php echo $_GET['idsurat'];?>">
			<td> <input name="tambahfile" type="submit" class="button-table-edit" value="TAMBAH FILE"></input> </td>
		</form>
	</tr>

	<tr>
		<td>FILE</td>		
			<?php
				if(isset($_POST["idsurat"])) $temp= $_POST["idsurat"];
				if(isset($_GET["idsurat"])) $temp= $row["IDSURAT"];
				$sql="select * from file_surat where idsurat='".$temp."';";
				$result=$conn->query($sql);
			?>		
		<td>
		<?php
				if($row = $result->fetch_assoc()){
				echo $row['NAMAFILE'];
		?>	
		</td>
		<td align="center">

			<a class="button-table-hapus" href="hapusfile.php?namafile=<?php echo $row['NAMAFILE']; ?>&idsurat=<?php echo $_GET['idsurat'];?>">HAPUS</a> 

		</td>

		<?php
			} else {
		?>	
		<td></td>
		<?php
			}
		?>
		
			
	</tr>
		<?php
			while($row = $result->fetch_assoc()){
		?>

	<tr>
		<td></td>
		<td><?php echo $row['NAMAFILE']; ?></td>
		<td align="center">
			<a class="button-table-hapus" href="hapusfile.php?namafile=<?php echo $row['NAMAFILE']; ?>&idsurat=<?php echo $_GET['idsurat'];?>">HAPUS</a> 
		</td>
	</tr>

	<?php
		}
	?>
	
</table>
</center>

</body>
</html>