<?php
session_start();
	if(isset($_SESSION["username"]));
	else
		header('location:login.php');
?>

<!DOCTYPE html>
<html>
<head>
	<title>Ex: Aplikasi Arsip</title>
</head>

<body>

<style>
ul{
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #46add9; /* light blue */
}

li{
	display: inline;
	float: left;
}

li a{
	display: block;
	color: white;
	text-align: center;
    padding: 14px 16px;
    background-color: #46add9; /* light blue */
    text-decoration: none;
}

li a:hover{
	background-color: #317997; /* dark blue */
}

table {
	border-collapse: collapse;
	border: 1px solid gray;
}

td {
	padding: 6px;
}

th {
	padding: 6px;
	background-color: #008ab8; /* blue */
	color: white;
}

tr:nth-child(even) {
	background-color: #f2f2f2
}

.box {
	border-radius: 4px;
	width: 70%;
}   

.button {
	background-color: green; /* blue */	
	border: none;
	border-radius: 5px;
	color: white;
	padding: 11px 11px;
	font-size: 14px;
}	

}
</style>

<ul>
	<li> <a href="index.php">HOME</a> </li>
	<li> <a href="surat.php?q=1">SURAT MASUK</a> </li>
	<li> <a href="surat.php?q=0">SURAT KELUAR</a> </li>
	<li> <a style="display:<?php if($_SESSION['username']!='admin')echo "none"; ?>" href="admin.php">KELOLA ADMIN</a> </li>
	<li style="float:right"> <a href="logout.php">LOGOUT</a> </li>
</ul>

<br>
<br>
<br>

<center>
<table>
<form action="upload.php" method="post" enctype="multipart/form-data">

	<tr>
		<td>NO AGENDA</td>
		<td> <input name="noagenda" type="text" class="box" required/> </td>
	</tr>
	<tr>
		<td>NO SURAT</td>
		<td> <input name="nosurat" class="box" type="text"  /> </td>
	</tr>
	<tr>
		<td><?php if($_GET['q'])echo "ASAL";else echo "TUJUAN"; ?></td>
		<td> <input name="asaltujuan" class="box" type="text"/> </td>
	</tr>
	<tr>
		<td>ISI RINGKAS</td>
		<td> <textarea rows="5" cols="20" name="isiringkas" class="box" type="text"></textarea> </td>
	</tr>
	<tr>
		<td>KODE KLARIFIKASI</td>
		<td> <input name="kodeklarifikasi" class="box" type="text"/> </td>
	</tr>
	<tr>	
		<td>TANGGAL ARSIP</td>
		<td> <input name="tanggal" class="box" type="date"/> </td>
	</tr>
	<tr>	
		<td>TANGGAL SURAT</td>
		<td> <input name="tanggalsurat" class="box" type="date"/> </td>
	</tr>
	<tr>	
		<td>KETERANGAN</td>
		<td> <textarea rows="5" cols="20" name="keterangan" class="box" type="text"></textarea> </td>
	</tr>
	<tr>
		<td><input type="hidden" name="suratmasuk" value="<?php echo $_GET['q']; ?>"/> </td>
		<td><input type="hidden" name="idsurat" value="<?php echo $_GET['n']; ?>"/> </td>
	</tr>
	<tr>
		<td>SELECT FILE TO UPLOAD</td>
		<td> <input type="file" name="fileToUpload[]" multiple="multiple"></input> </td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" class="button" value="Simpan" ></input></td>
	</tr>
</form>
</table>
</center>

</body>
</html>